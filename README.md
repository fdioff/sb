## Description

According to this task https://gist.github.com/osamakhn/a20088c2cc45447ecd941ca121a8358b.

1. There is a wrap for a WINAPI NamedPipe API - **pipe**.
2. **pipe** can work synchronous and asynchronous.
3. There are four projects - common, server, client, test.

## **common** project. static library

Uses boost::serialization to make **serializable** enabled to send/receive by **pipe**.

Include **pipe** class to implement sync/async data transfering by NamedPipe API, 
**serializable** class to create it on **server** and send to **client** by **pipe**,
**serializable_helper** to call certain function of **serializable** object.

## **server** project. executable

Uses **common** as static lib.

Create **pipe** object to send and receive data from **client**.

## **client** project

Uses **common** as static lib.

Create **pipe** object to send and receive data from **server**.

## **test** project. executable

Uses **common** as static lib.

Uses google test to test some pieces of **pipe**, **serializable** and **serializable_helper**.

## Build

run cmake.

google test included to repo as submodule.

all you need is set boost's variables.

I'm using boost 1.68.0 version, that's why I've set cmake 3.13 version.


## Description of work

1. **server** starts, creates **pipe** object, waits for connection from **client**.
2. **client** starts, creates **pipe** object, connects to **server**.
3. **client** send first data packet [header[message type | whole message size] | data] (see streambase::pipe::command in common/pipe.h) to inform **server** about waiting **serializable** object.
4. **server** creates **serializable** object and sends to **client**, calls serializable::hash()
5. **client** send new command to update **serializable** object's field by sending **serializable_helper** object. **client** update its **serializable** object and calls serializable::hash().
6. **server** receive **serializable_helper** object and update its **serializable** object according to **serializable_helper** fields and calls serializable::hash().
7. serializable::hash() on **server** and **client** have to show same data in console output.
8. **server** and **client** show 'done' in console output. both close their **pipe** objects.

## Run

1. call 'server.exe 0' (for async processing) or 'server.exe 1' (for sync processing) first.
2. call 'client.exe 0' (for async processing) or 'client.exe 1' (for sync processing) after. **client** will not wait for **server** running.
3. text in both consoles have to be the same.
