
#ifndef _SERIALIZABLE_
#define _SERIALIZABLE_

#include <cstdint>
#include <ostream>
#include <boost\serialization\access.hpp>
#include <boost\archive\binary_iarchive.hpp>
#include <boost\archive\binary_oarchive.hpp>
#include <string>

namespace streambase
{

class call_serializable_helper
{
public:
	call_serializable_helper();
	virtual ~call_serializable_helper();
	enum func
	{
		nothing,
		set_int,
		set_float,
		set_string,
	};
	func func_name;
	int set_int_arg0;
	float set_float_arg0;
	std::string set_string_arg0;
private:
	void serialize(boost::archive::binary_iarchive & ar, const unsigned int);
	void serialize(boost::archive::binary_oarchive & ar, const unsigned int);
private:
	friend class boost::serialization::access;
};

class serializable
{
public:
	serializable();
	explicit serializable(const uint32_t first, const float second, const std::string & third);
	virtual ~serializable();
	double hash() const;
	void set_int(const uint32_t v);
	void set_float(const float v);
	void set_string(const std::string & v);
private:
	friend std::ostream & operator<<(std::ostream & os, const serializable & s);
	void serialize(boost::archive::binary_iarchive & ar, const unsigned int);
	void serialize(boost::archive::binary_oarchive & ar, const unsigned int);
private:
	uint32_t _Int;
	float _Float;
	std::string _String;
	friend class boost::serialization::access;
};

}//namespace streambase

#endif _SERIALIZABLE_