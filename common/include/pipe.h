
#ifndef _PIPE_
#define _PIPE_

#include <functional>
#include <mutex>
#include <deque>
#include <future>
#include <string>
#include <Windows.h>
#include <chrono>
#include <memory>
#include <atomic>
#include <boost\noncopyable.hpp>

//
//    pipe class for send/receive data through NamedPipe API
//    Supports sync and async calls
//
//    'wait' function waits until all async calls end
//    You can call it at the end of scope or program
//
//    Not thread safe

namespace streambase
{

class pipe : public std::enable_shared_from_this<pipe>, public boost::noncopyable
{
public:
	enum class e_type
	{
		e_type_invalid,
		e_type_server,
		e_type_client,
	};
public:
	explicit pipe(e_type type, const std::string & pipe_name);
	virtual ~pipe();
	bool connect();
	void connect_async(std::function<void(bool, std::shared_ptr<pipe>)> on_connect);
	bool accept();
	void accept_async(std::function<void(bool, std::shared_ptr<pipe>)> on_accept);
	size_t read(void * pBuffer, size_t size);
	void read_async(void * buffer, size_t size, std::function<void(size_t, std::shared_ptr<pipe>)> on_read);
	size_t write(void * buffer, size_t size);
	void write_async(void * buffer, size_t size, std::function<void(size_t, std::shared_ptr<pipe>)> on_write);
	void wait();
	bool valid() const;
	void set_opaque(void * opaque);
	void * opaque() const;
public:
	enum class e_commands
	{
		e_commands_invalid,
		e_commands_int8,
		e_commands_int16,
		e_commands_int32,
		e_commands_int64,
		e_commands_uint8,
		e_commands_uint16,
		e_commands_uint32,
		e_commands_uint64,
		e_commands_str,
		e_commands_wstr,
		e_commands_raw,
		e_commands_class_serialization,
		e_commands_class_member,
	};

	struct command
	{
		e_commands cmd : 8;//for x64 memory alignment
		uint64_t size : 8;
	};
private:
	bool _Connect_wrap(std::function<void(bool, std::shared_ptr<pipe>)> func = nullptr);
	bool _Accept_wrap(std::function<void(bool, std::shared_ptr<pipe>)> func = nullptr);
	size_t _Read_wrap(void * buffer, size_t size, std::function<void(size_t, std::shared_ptr<pipe>)> func = nullptr);
	size_t _Write_wrap(void * buffer, size_t size, std::function<void(size_t, std::shared_ptr<pipe>)> func = nullptr);
	bool _Connect_impl();
	bool _Accept_impl();
	size_t _Read_impl(void * buffer, size_t size);
	size_t _Write_impl(void * buffer, size_t size);
	bool _Is_right_pipe_name();
	bool _Wait_impl();
private:
	const e_type _Type;
	std::string _Pipe_name;
	mutable std::mutex _Mtx_callbacks;
	std::mutex _Mtx_read;
	std::mutex _Mtx_write;
	std::deque<std::future<bool>> _Callbacks_bool;
	std::deque<std::future<size_t>> _Callbacks_sizet;
	HANDLE _Pipe_handle;
	const size_t _Max_attempts = 10;
	const size_t _Time_to_wait_connect_sec = 1;
	const size_t _Time_to_wait_after_accept_sec = 1;
	const size_t _Max_time_to_wait_connect_ms = 30000;
	const size_t _Max_instances = 2;
	const size_t _Pipe_buffer_size = 1024;
	const size_t _Max_pipe_name_length = 256;
	bool _Connected;
	bool _Accepted;
	std::atomic<bool> _Wait_called;
	bool _Valid;
	void * _Opaque;
};

}//namespace streambase

#endif _PIPE_

