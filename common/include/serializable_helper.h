#ifndef _SERIALIZABLE_HELPER_
#define _SERIALIZABLE_HELPER_

#include <memory>
#include "serializable.h"
#include <sstream>
#include <boost\archive\binary_oarchive.hpp>
#include <boost\archive\binary_iarchive.hpp>
#include <iostream>

namespace streambase
{
using namespace std;

template<class T>
bool save(unique_ptr<char[]> * buffer, size_t * size, T * object_to_serialize)
{
	if (!buffer)
		return false;

	if (!size)
		return false;

	if (!object_to_serialize)
		return false;

	try
	{
		stringstream stream;
		boost::archive::binary_oarchive binary_output(stream);
		binary_output << *object_to_serialize;
		*size = static_cast<size_t>(stream.tellp());
		buffer->reset(new char[*size]);
		stream.read(buffer->get(), *size);
		return true;
	}
	catch (exception & exc)
	{
		cout << "exception in streambase::save: " << exc.what() << endl;
	}
	return false;
}

template<class T>
bool load(const void * const buffer, size_t size, T * object_from_serialize)
{
	if (!buffer)
		return false;

	if (!size)
		return false;

	if (!object_from_serialize)
		return false;

	try
	{
		stringstream stream;
		stream.write(static_cast<const char *>(buffer), size);
		boost::archive::binary_iarchive binary_input(stream);

		binary_input >> *object_from_serialize;
		return true;
	}
	catch (exception & exc)
	{
		cout << "exception in streambase::load: " << exc.what() << endl;
	}
	return false;
}

}

#endif _SERIALIZABLE_HELPER_