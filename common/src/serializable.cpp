#include "Serializable.h"
#include <algorithm>

namespace streambase
{

serializable::serializable() :
	_Int(0)
	, _Float(0.0f)
{
}

serializable::serializable(const uint32_t first, const float second, const std::string & third) :
	_Int(first)
	, _Float(second)
	, _String(third)
{
}


serializable::~serializable()
{
}

double serializable::hash() const
{
	double result = static_cast<double>(_Int) + static_cast<double>(_Float);
	std::for_each(_String.cbegin(), _String.cend(), [&result](const char c) {result += static_cast<double>(c); });
	return result;
}

void serializable::set_int(const uint32_t v)
{
	_Int = v;
}

void serializable::set_float(const float v)
{
	_Float = v;
}

void serializable::set_string(const std::string & v)
{
	_String = v;
}

std::ostream & operator<<(std::ostream & os, const serializable & s)
{
	return os << s.hash();
}

void serializable::serialize(boost::archive::binary_iarchive & ar, const unsigned int)
{
	ar & _Int;
	ar & _Float;
	ar & _String;
}

void serializable::serialize(boost::archive::binary_oarchive & ar, const unsigned int)
{
	ar & _Int;
	ar & _Float;
	ar & _String;
}

call_serializable_helper::call_serializable_helper() :
	func_name(func::nothing)
	, set_int_arg0(0)
	, set_float_arg0(0.0f)
{
}

call_serializable_helper::~call_serializable_helper()
{
}

void call_serializable_helper::serialize(boost::archive::binary_iarchive & ar, const unsigned int)
{
	ar & func_name & set_int_arg0 & set_float_arg0 & set_string_arg0;
}

void call_serializable_helper::serialize(boost::archive::binary_oarchive & ar, const unsigned int)
{
	ar & func_name & set_int_arg0 & set_float_arg0 & set_string_arg0;
}

}//namespace streambase