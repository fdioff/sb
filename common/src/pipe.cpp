#include "pipe.h"
#include <iostream>

using namespace std;
namespace streambase
{
pipe::pipe(e_type type, const std::string & pipe_name) :
	_Type(type)
	, _Pipe_name(pipe_name)
	, _Pipe_handle(INVALID_HANDLE_VALUE)
	, _Connected(false)
	, _Accepted(false)
	, _Wait_called(false)
	, _Valid(false)
	, _Opaque(nullptr)
{
	if (_Type != e_type::e_type_server && _Type != e_type::e_type_client)
	{
		cout << "incorrect e_type argument" << endl;
		return;
	}
	if (!this->_Is_right_pipe_name())
	{
		cout << "pipe_name is incorrect." << endl <<
			"It have to be \"\\\\.\\pipe\\...\"," << endl <<
			"length is equivalent or less than 256 symbols." << endl <<
			"pipe_name cannot contain '/' symbol." << endl;
		return;
	}
	_Valid = true;
}

pipe::~pipe()
{
	if (_Pipe_handle != INVALID_HANDLE_VALUE)
	{
		if (_Type == e_type::e_type_server)
			DisconnectNamedPipe(_Pipe_handle);
		CloseHandle(_Pipe_handle);
	}
}

bool pipe::connect()
{
	if (!_Valid)
		return false;

	return this->_Connect_wrap();
}

void pipe::connect_async(std::function<void(bool, std::shared_ptr<pipe>)> on_connect)
{
	if (!_Valid)
		return;

	if (_Wait_called.load())
		return;

	lock_guard<mutex> lock(_Mtx_callbacks);
	_Callbacks_bool.emplace_back(async(&pipe::_Connect_wrap, this, on_connect));
}

bool pipe::accept()
{
	if (!_Valid)
		return false;

	return this->_Accept_wrap();
}

void pipe::accept_async(std::function<void(bool, std::shared_ptr<pipe>)> on_accept)
{
	if (!_Valid)
		return;

	if (_Wait_called.load())
		return;

	lock_guard<mutex> lock(_Mtx_callbacks);
	_Callbacks_bool.emplace_back(async(&pipe::_Accept_wrap, this, on_accept));
}

size_t pipe::read(void * buffer, size_t size)
{
	if (!_Valid)
		return 0;

	return this->_Read_wrap(buffer, size);
}

void pipe::read_async(void * buffer, size_t size, std::function<void(size_t, std::shared_ptr<pipe>)> on_read)
{
	if (!_Valid)
		return;

	if (_Wait_called.load())
		return;

	lock_guard<mutex> lock(_Mtx_callbacks);
	_Callbacks_sizet.emplace_back(async(&pipe::_Read_wrap, this, buffer, size, on_read));
}

size_t pipe::write(void * buffer, size_t size)
{
	if (!_Valid)
		return 0;

	return this->_Write_wrap(buffer, size);
}

void pipe::write_async(void * buffer, size_t size, std::function<void(size_t, std::shared_ptr<pipe>)> on_write)
{
	if (!_Valid)
		return;

	if (_Wait_called.load())
		return;

	lock_guard<mutex> lock(_Mtx_callbacks);
	_Callbacks_sizet.emplace_back(async(&pipe::_Write_wrap, this, buffer, size, on_write));
}

void pipe::wait()
{
	while (true)
	{
		bool result = this->_Wait_impl();
		if (result)
			break;
		this_thread::sleep_for(chrono::milliseconds(1));
	}

	_Wait_called.exchange(true);

	{
		lock_guard<mutex> lock(_Mtx_callbacks);
		_Callbacks_bool.clear();
		_Callbacks_sizet.clear();
	}
}

bool pipe::valid() const
{
	return _Valid;
}

void pipe::set_opaque(void * opaque)
{
	_Opaque = opaque;
}

void * pipe::opaque() const
{
	return _Opaque;
}

bool pipe::_Connect_impl()
{
	if (_Type != e_type::e_type_client)
		return false;

	if (_Connected)
		return false;

	int result = WaitNamedPipeA(_Pipe_name.data(), _Max_time_to_wait_connect_ms);
	if (!result)
		return false;

	for (size_t i = 0; i < _Max_attempts; ++i)
	{
		_Pipe_handle = CreateFileA(_Pipe_name.data(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, 0);
		if (_Pipe_handle != INVALID_HANDLE_VALUE)
		{
			break;
		}
		this_thread::sleep_for(chrono::seconds(_Time_to_wait_connect_sec));
	}

	if (_Pipe_handle == INVALID_HANDLE_VALUE)
		return false;

	if (result)
		_Connected = true;

	return true;
}

bool pipe::_Accept_impl()
{
	if (_Type != e_type::e_type_server)
		return false;

	if (_Accepted)
		return false;

	_Pipe_handle = CreateNamedPipeA(_Pipe_name.data(), PIPE_ACCESS_DUPLEX, PIPE_TYPE_BYTE, _Max_instances,
		_Pipe_buffer_size, _Pipe_buffer_size, NMPWAIT_USE_DEFAULT_WAIT, nullptr);

	if (_Pipe_handle == INVALID_HANDLE_VALUE)
		return false;

	int result = ConnectNamedPipe(_Pipe_handle, nullptr);

	//prevent race condition
	this_thread::sleep_for(chrono::seconds(_Time_to_wait_after_accept_sec));
	
	if (result)
		_Accepted = true;

	return result != 0;
}

size_t pipe::_Read_impl(void * buffer, size_t size)
{
	if (_Pipe_handle == INVALID_HANDLE_VALUE)
		return 0;

	DWORD readed = 0;
	
	{
		lock_guard<mutex> lock(_Mtx_read);
		ReadFile(_Pipe_handle, buffer, size, &readed, nullptr);
	}

	return readed;
}

size_t pipe::_Write_impl(void * buffer, size_t size)
{
	if (_Pipe_handle == INVALID_HANDLE_VALUE)
		return 0;

	DWORD writed = 0;
	
	{
		lock_guard<mutex> lock(_Mtx_write);
		WriteFile(_Pipe_handle, buffer, size, &writed, nullptr);
	}

	return writed;
}

bool pipe::_Is_right_pipe_name()
{
	if (_Pipe_name.size() > _Max_pipe_name_length)
		return false;

	if (_Pipe_name.find("/") != string::npos)
		return false;

	//pipe name is non case sensitive
	string temp(_Pipe_name);
	transform(temp.begin(), temp.end(), temp.begin(), tolower);
	const string begin = "\\\\.\\pipe\\";
	if (temp.find(begin) == 0)
		return true;
	return false;
}
bool pipe::_Wait_impl()
{
	lock_guard<mutex> lock(_Mtx_callbacks);

	size_t ready = 0;

	for (auto & cb : _Callbacks_bool)
	{
		auto res = cb.wait_for(chrono::nanoseconds(1));
		if (res == future_status::ready)
			++ready;
	}

	for (auto & cb : _Callbacks_sizet)
	{
		auto res = cb.wait_for(chrono::nanoseconds(1));
		if (res == future_status::ready)
			++ready;
	}

	if (ready == (_Callbacks_bool.size() + _Callbacks_sizet.size()))
		return true;

	return false;
}
bool pipe::_Connect_wrap(std::function<void(bool, std::shared_ptr<pipe>)> func)
{
	bool result = this->_Connect_impl();
	if (func)
	{
		func(result, shared_from_this());
	}
	return result;
}
bool pipe::_Accept_wrap(std::function<void(bool, std::shared_ptr<pipe>)> func)
{
	bool result = this->_Accept_impl();
	if (func)
	{
		func(result, shared_from_this());
	}
	return result;
}
size_t pipe::_Read_wrap(void * buffer, size_t size, std::function<void(size_t, std::shared_ptr<pipe>)> func)
{
	size_t result = this->_Read_impl(buffer, size);
	if (func)
	{
		func(result, shared_from_this());
	}
	return result;
}
size_t pipe::_Write_wrap(void * buffer, size_t size, std::function<void(size_t, std::shared_ptr<pipe>)> func)
{
	size_t result = this->_Write_impl(buffer, size);
	if (func)
	{
		func(result, shared_from_this());
	}
	return result;
}

}//namespace streambase
