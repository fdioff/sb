#ifndef _PIPE_TEST_
#define _PIPE_TEST_

#include <gtest\gtest.h>
#include <pipe.h>
#include <string>
#include <memory>

TEST(PipeTest, PipeCorrect)
{
	std::string pipe_name = "\\\\.\\pipe\\jfjfjfjfsdlvknsdjv";
	streambase::pipe p(streambase::pipe::e_type::e_type_server, pipe_name);
	EXPECT_TRUE(p.valid());
}

TEST(PipeTest, PipeTypeIncorrect)
{
	std::string pipe_name = "\\\\.\\pipe\\jfjfjfjfsdlvknsdjv";
	streambase::pipe p(streambase::pipe::e_type::e_type_invalid, pipe_name);
	EXPECT_FALSE(p.valid());
}

TEST(PipeTest, PipeNameWrong)
{
	std::string wrong_pipe_name = "\\\\.\\pipi\\jfjfjfjf";
	streambase::pipe p(streambase::pipe::e_type::e_type_client, wrong_pipe_name);
	EXPECT_FALSE(p.valid());
}

TEST(PipeTest, ServerCannotConnect)
{
	std::string pipe_name = "\\\\.\\pipe\\jfjfjfjfsdlvknsdjv";
	streambase::pipe p(streambase::pipe::e_type::e_type_server, pipe_name);
	EXPECT_FALSE(p.connect());
}

TEST(PipeTest, ClientCannotAccept)
{
	std::string pipe_name = "\\\\.\\pipe\\jfjfjfjfsdlvknsdjv";
	streambase::pipe p(streambase::pipe::e_type::e_type_client, pipe_name);
	EXPECT_FALSE(p.accept());
}
#endif _PIPE_TEST_