#ifndef _SERIALIZABLE_TEST_
#define _SERIALIZABLE_TEST_

#include <gtest\gtest.h>
#include <serializable.h>
#include <serializable_helper.h>

TEST(SerializableTest, CheckParams)
{
	const int _int = 1098;
	const float _float = 6432.43f;
	const std::string _string = "some text";
	streambase::serializable first;
	first.set_int(_int);
	first.set_float(_float);
	first.set_string(_string);
	streambase::serializable second(_int, _float, _string);
	EXPECT_DOUBLE_EQ(first.hash(), second.hash());
}

TEST(SerializableTest, ChangeHash)
{
	int _int = 1098;
	float _float = 6432.43f;
	std::string _string = "some text";
	streambase::serializable _serializable(_int, _float, _string);
	double first = _serializable.hash();
	
	_serializable.set_int(_int * 10);
	double second = _serializable.hash();
	
	_serializable.set_int(_int);
	_serializable.set_float(_float * 10.0f);
	double third = _serializable.hash();

	_serializable.set_int(_int);
	_serializable.set_float(_float);
	_string.append(_string);
	_serializable.set_string(_string);
	double fourth = _serializable.hash();

	EXPECT_NE(first, second);
	EXPECT_NE(first, third);
	EXPECT_NE(first, fourth);
}

TEST(SerializableTest, SerializableSaveLoad)
{
	int _int = 1098;
	float _float = 6432.43f;
	std::string _string = "some text";
	streambase::serializable first(_int, _float, _string);
	streambase::serializable second;

	std::unique_ptr<char[]> buffer;
	size_t size = 0;
	bool result = streambase::save(&buffer, &size, &first);
	ASSERT_TRUE(result);
	ASSERT_NE(buffer.get(), nullptr);
	ASSERT_NE(size, 0);

	result = streambase::load(buffer.get(), size, &second);
	ASSERT_TRUE(result);
	EXPECT_EQ(first.hash(), second.hash());
}

TEST(SerializableTest, SerializableHelperSaveLoad)
{
	streambase::call_serializable_helper first, second;
	first.func_name = streambase::call_serializable_helper::func::set_float;
	first.set_float_arg0 = -345.33f;
	first.set_int_arg0 = 256;
	first.set_string_arg0 = "caramba";

	std::unique_ptr<char[]> buffer;
	size_t size = 0;
	bool result = streambase::save(&buffer, &size, &first);
	ASSERT_TRUE(result);
	ASSERT_NE(buffer.get(), nullptr);
	ASSERT_NE(size, 0);

	result = streambase::load(buffer.get(), size, &second);
	ASSERT_TRUE(result);
	EXPECT_EQ(first.func_name, second.func_name);
	EXPECT_EQ(first.set_int_arg0, second.set_int_arg0);
	EXPECT_STREQ(first.set_string_arg0.data(), second.set_string_arg0.data());
	EXPECT_FLOAT_EQ(first.set_float_arg0, second.set_float_arg0);
}


#endif _SERIALIZABLE_TEST_