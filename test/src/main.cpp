
#include <gtest\gtest.h>
#include "pipe_test.h"
#include "serializable_test.h"

int main(int argc, char * argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

