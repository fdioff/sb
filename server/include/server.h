#ifndef _SERVER_
#define _SERVER_

#include <pipe.h>
#include <memory>
#include <string>
#include <serializable_helper.h>
#include <boost\noncopyable.hpp>

namespace streambase
{

class server : public boost::noncopyable
{
public:
	server();
	virtual ~server();
	void run_sync();
	void run_async();
private:
	static void _On_accept(bool accepted, shared_ptr<pipe> p);
	static void _On_read_command_class_serialization(size_t size, shared_ptr<pipe> p);
	static void _On_write_command_class_serialization(size_t size, shared_ptr<pipe> p);
	static void _On_write_serialization(size_t size, shared_ptr<pipe> p);
	static void _On_read_command_class_member(size_t size, shared_ptr<pipe> p);
	static void _On_read_class_member(size_t size, shared_ptr<pipe> p);
private:
	std::shared_ptr<pipe> _Pipe;
	const std::string _Pipe_name;
private://for current async calls
	pipe::command _Command;
	serializable _Serializable_object;
	unique_ptr<char[]> _Buffer;
	size_t _Size;
	call_serializable_helper _Helper;
};

}//namespace streambase

#endif _SERVER_