
#include "server.h"
#include <exception>
#include <iostream>
#include <string>

using namespace std;
using namespace streambase;

int main(int argc, char * argv[])
{	
	try
	{
		if (argc < 2)
		{
			cout << "Too few arguments. Minimum is 2. Use '0' for async processing, '1' for sync" << endl;
			return 1;
		}
		server s;
		int arg = stoi(argv[1]);
		switch (arg)
		{
		case 0:
			s.run_async();
			break;
		case 1:
			s.run_sync();
			break;
		default:
			cout << "You're using wrong parameter. Use '0' for async processing, '1' for sync" << endl;
			return 2;
		}
	}
	catch (exception & exc)
	{
		cout << "error in main: " << exc.what() << endl;
		return 3;
	}
	return 0;
}
