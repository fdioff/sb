#include "server.h"
#include <pipe.h>
#include <string>
#include <iostream>
#include <serializable.h>
#include <serializable_helper.h>
#include <boost\process\pipe.hpp>

namespace streambase
{

using namespace std;

server::server() :
	_Pipe_name("\\\\.\\pipe\\streambasepipename")
	, _Command()
	, _Serializable_object()
	, _Size(0)
	, _Helper()
{
}

server::~server()
{
}

void server::run_sync()
{
	try
	{
		_Pipe.reset(new pipe(pipe::e_type::e_type_server, _Pipe_name.data()));
		if (!_Pipe->valid())
		{
			cout << "Incorrect pipe name" << endl;
			return;
		}
		if (!_Pipe->accept())
		{
			cout << "pipe::accept failed" << endl;
			return;
		}

		pipe::command command = {};
		size_t size = sizeof(command);
		size_t read = _Pipe->read(&command, size);

		if (read != size)
		{
			cout << "pipe::read failed" << endl;
			return;
		}

		if(command.cmd != pipe::e_commands::e_commands_class_serialization || command.size)
		{
			cout << "pipe::read has incorrect data" << endl;
			return;
		}

		string trash = "qwertyulxklkfnvb,cskjvwioru9weirof";
		serializable serializable_object(123, 456.0f, trash);
		unique_ptr<char[]> buffer;
		if(!save(&buffer, &size, &serializable_object))
		{
			cout << "cannot save streambase::serializable object" << endl;
			return;
		}

		command.size = size;
		size_t write = _Pipe->write(&command, sizeof(command));

		if (write != sizeof(command))
		{
			cout << "pipe::write failed" << endl;
			return;
		}

		write = _Pipe->write(buffer.get(), size);

		if (write != size)
		{
			cout << "pipe::write failed" << endl;
			return;
		}
		
		cout << "hash is " << serializable_object << endl;

		read = _Pipe->read(&command, sizeof(command));

		if (read != sizeof(command))
		{
			cout << "2nd pipe::read failed" << endl;
			return;
		}

		if (command.cmd != pipe::e_commands::e_commands_class_member || !command.size)
		{
			cout << "2nd pipe::read has incorrect data" << endl;
			return;
		}

		size = command.size;
		buffer.reset(new char[size]);
		
		read = _Pipe->read(buffer.get(), size);

		if (read != size)
		{
			cout << "3rd pipe::read failed" << endl;
			return;
		}

		call_serializable_helper helper;
		if(!load(buffer.get(), size, &helper))
		{
			cout << "cannot load streambase::call_serializable_helper object" << endl;
			return;
		}

		switch (helper.func_name)
		{
			case call_serializable_helper::func::set_int:
			serializable_object.set_int(helper.set_int_arg0);
			break;
			case call_serializable_helper::func::set_float:
				serializable_object.set_float(helper.set_float_arg0);
			break;
			case call_serializable_helper::func::set_string:
				serializable_object.set_string(helper.set_string_arg0);
			break;
		default:
			cout << "Incorrect helper.func_name" << endl;
			return;
		}

		cout << "new hash is " << serializable_object << endl;
		cout << "done" << endl;
	}
	catch (exception & exc)
	{
		cout << "error in server::run_sync: " << exc.what() << endl;
	}
}

void server::run_async()
{
	try
	{
		_Pipe.reset(new pipe(pipe::e_type::e_type_server, _Pipe_name.data()));
		if (!_Pipe->valid())
		{
			cout << "Incorrect pipe name" << endl;
			return;
		}
		_Pipe->set_opaque(this);
		_Pipe->accept_async(_On_accept);
		_Pipe->wait();
	}
	catch (exception & exc)
	{
		cout << "error in server::run_sync: " << exc.what() << endl;
	}
}

void server::_On_accept(bool accepted, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	server * opaque = reinterpret_cast<server *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (!accepted)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	p->read_async(&opaque->_Command, sizeof(opaque->_Command), _On_read_command_class_serialization);
}

void server::_On_read_command_class_serialization(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	server * opaque = reinterpret_cast<server *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != sizeof(opaque->_Command))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (opaque->_Command.cmd != pipe::e_commands::e_commands_class_serialization || opaque->_Command.size)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	string trash = "qwertyulxklkfnvb,cskjvwioru9weirof";
	opaque->_Serializable_object = serializable(123, 456.0f, trash);

	if (!save(&opaque->_Buffer, &opaque->_Size, &opaque->_Serializable_object))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	opaque->_Command.size = opaque->_Size;
	p->write_async(&opaque->_Command, sizeof(opaque->_Command), _On_write_command_class_serialization);
}

void server::_On_write_command_class_serialization(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	server * opaque = reinterpret_cast<server *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != sizeof(opaque->_Command))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	cout << "hash is " << opaque->_Serializable_object << endl;

	p->write_async(opaque->_Buffer.get(), opaque->_Size, _On_write_serialization);

}

void server::_On_write_serialization(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	server * opaque = reinterpret_cast<server *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != opaque->_Size)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	p->read_async(&opaque->_Command, sizeof(opaque->_Command), _On_read_command_class_member);
}

void server::_On_read_command_class_member(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	server * opaque = reinterpret_cast<server *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != sizeof(opaque->_Command))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (opaque->_Command.cmd != pipe::e_commands::e_commands_class_member || !opaque->_Command.size)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	opaque->_Size = opaque->_Command.size;

	opaque->_Buffer.reset(new char[opaque->_Size]);

	p->read_async(opaque->_Buffer.get(), opaque->_Size, _On_read_class_member);
}

void server::_On_read_class_member(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	server * opaque = reinterpret_cast<server *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != opaque->_Size)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (!load(opaque->_Buffer.get(), size, &opaque->_Helper))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	switch (opaque->_Helper.func_name)
	{
	case call_serializable_helper::func::set_int:
		opaque->_Serializable_object.set_int(opaque->_Helper.set_int_arg0);
		break;
	case call_serializable_helper::func::set_float:
		opaque->_Serializable_object.set_float(opaque->_Helper.set_float_arg0);
		break;
	case call_serializable_helper::func::set_string:
		opaque->_Serializable_object.set_string(opaque->_Helper.set_string_arg0);
		break;
	default:
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}
	}

	cout << "new hash is " << opaque->_Serializable_object << endl;
	cout << "done" << endl;
}

}