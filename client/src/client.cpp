#include "client.h"
#include <pipe.h>
#include <string>
#include <iostream>
#include <serializable.h>

namespace streambase
{

client::client() :
	_Pipe_name("\\\\.\\pipe\\streambasepipename")
	, _Command()
	, _Serializable_object()
	, _Size(0)
	, _Helper()
{

}

client::~client()
{

}

void client::run_sync()
{
	try
	{
		_Pipe.reset(new pipe(pipe::e_type::e_type_client, _Pipe_name.data()));
		if (!_Pipe->valid())
		{
			cout << "Invalid pipe name" << endl;
			return;
		}
		
		if (!_Pipe->connect())
		{
			cout << "streambase::pipe::connect failed" << endl;
			return;
		}

		pipe::command command = {};
		command.cmd = pipe::e_commands::e_commands_class_serialization;
		command.size = 0;
		
		size_t write = _Pipe->write(&command, sizeof(command));

		if (write != sizeof(command))
		{
			cout << "1st streambase::pipe::write failed" << endl;
			return;
		}

		size_t read = _Pipe->read(&command, sizeof(command));

		if (read != sizeof(command))
		{
			cout << "1st streambase::pipe::read failed" << endl;
			return;
		}

		if (command.cmd != pipe::e_commands::e_commands_class_serialization || !command.size)
		{
			cout << "1st streambase::pipe::read has incorrect data" << endl;
			return;
		}

		size_t size = command.size;
		unique_ptr<char[]> buffer(new char[size]);
		memset(buffer.get(), 0, size);

		read = _Pipe->read(buffer.get(), size);
		if (read != size)
		{
			cout << "2nd streambase::pipe::read failed" << endl;
			return;
		}

		serializable ser;
		if (!load(buffer.get(), size, &ser))
		{
			cout << "cannot load streambase::serializable object" << endl;
			return;
		}

		cout << "hash is " << ser << endl;

		call_serializable_helper caller;
		caller.func_name = call_serializable_helper::func::set_float;
		caller.set_float_arg0 = 951.357f;
		if (!save(&buffer, &size, &caller))
		{
			cout << "cannot save streambase::call_serializable_helper object" << endl;
			return;
		}

		command.cmd = pipe::e_commands::e_commands_class_member;
		command.size = size;

		write = _Pipe->write(&command, sizeof(command));
		
		if (write != sizeof(command))
		{
			cout << "2nd streambase::pipe::write failed" << endl;
			return;
		}

		write = _Pipe->write(buffer.get(), size);

		if (write != size)
		{
			cout << "3rd streambase::pipe::write failed" << endl;
			return;
		}

		ser.set_float(caller.set_float_arg0);
		cout << "new hash is " << ser << endl;
		cout << "done" << endl;
	}
	catch (exception & exc)
	{
		cout << "error in client::run_sync: " << exc.what() << endl;
	}
}

void client::run_async()
{
	try
	{
		_Pipe.reset(new pipe(pipe::e_type::e_type_client, _Pipe_name.data()));
		if (!_Pipe->valid())
		{
			cout << "Incorrect pipe name" << endl;
			return;
		}
		_Pipe->set_opaque(this);
		_Pipe->connect_async(_On_connect);
		_Pipe->wait();
	}
	catch (exception & exc)
	{
		cout << "error in client::run_async: " << exc.what() << endl;
	}
}

void client::_On_connect(bool connected, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	client * opaque = reinterpret_cast<client *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (!connected)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	opaque->_Command.cmd = pipe::e_commands::e_commands_class_serialization;
	opaque->_Command.size = 0;
	p->write_async(&opaque->_Command, sizeof(opaque->_Command), _On_write_command_class_serialization);
}

void client::_On_write_command_class_serialization(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	client * opaque = reinterpret_cast<client *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != sizeof(opaque->_Command))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	p->read_async(&opaque->_Command, sizeof(opaque->_Command), _On_read_command_class_serialization);
}

void client::_On_read_command_class_serialization(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	client * opaque = reinterpret_cast<client *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != sizeof(opaque->_Command))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (opaque->_Command.cmd != pipe::e_commands::e_commands_class_serialization || !opaque->_Command.size)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	opaque->_Size = opaque->_Command.size;
	try
	{
		opaque->_Buffer.reset(new char[opaque->_Size]);
	}
	catch (exception & exc)
	{
		cout << "opaque->_Buffer.reset() in on_read_command_class_serialization failed with exception " << exc.what() << endl;
		return;
	}
	memset(opaque->_Buffer.get(), 0, opaque->_Size);

	p->read_async(opaque->_Buffer.get(), opaque->_Size, _On_read_class_serialization);
}

void client::_On_read_class_serialization(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	client * opaque = reinterpret_cast<client *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != opaque->_Size)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (!load(opaque->_Buffer.get(), size, &opaque->_Serializable_object))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	cout << "hash is " << opaque->_Serializable_object << endl;

	opaque->_Helper.func_name = call_serializable_helper::func::set_float;
	opaque->_Helper.set_float_arg0 = 951.357f;
	if (!save(&opaque->_Buffer, &opaque->_Size, &opaque->_Helper))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	opaque->_Command.cmd = pipe::e_commands::e_commands_class_member;
	opaque->_Command.size = opaque->_Size;

	p->write_async(&opaque->_Command, sizeof(opaque->_Command), _On_write_command_class_member);
}


void client::_On_write_command_class_member(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	client * opaque = reinterpret_cast<client *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != sizeof(opaque->_Command))
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	p->write_async(opaque->_Buffer.get(), opaque->_Size, _On_write_class_member);
}

void client::_On_write_class_member(size_t size, shared_ptr<pipe> p)
{
	if (!p)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	client * opaque = reinterpret_cast<client *>(p->opaque());
	if (!opaque)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	if (size != opaque->_Size)
	{
		cout << "error in " __FUNCTION__ " in line " << __LINE__ << endl;
		return;
	}

	opaque->_Serializable_object.set_float(opaque->_Helper.set_float_arg0);
	cout << "new hash is " << opaque->_Serializable_object << endl;
	cout << "done" << endl;
}

}//namespace streambase